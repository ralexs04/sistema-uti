# -*- coding: utf-8 -*-
{
    "name" : "GESTION UTI",
    "version" : "1.0",
    "depends" : [],
    'author': 'Raúl Gómez - Stalin Riofrío',
    "category": "Generic Modules/Human Resources",
    'website': 'www.unl.edu.ec',
    'description': 
                    """
                    Módulo Gestion UTI
                    """,
    'init_xml': [],
    'update_xml': [

               "security/groups.xml",
               "security/ir.model.access.csv",
               "views/menu_view.xml",
               "views/tarea_view.xml",
               "views/redes/tecnico/inventario/inventario_view.xml",
               "views/redes/tecnico/inventario/equipo_view.xml",
               "views/redes/tecnico/inventario/ubicacion_view.xml",
               "views/redes/tecnico/material/canaleta_view.xml",
               "views/redes/tecnico/material/conector_view.xml",
               
               "views/redes/tecnico/material/cable_view.xml",
               "views/redes/tecnico/material/rack_view.xml",
               "views/redes/tecnico/material/patchpanel_view.xml",
               "views/redes/tecnico/material/jack_view.xml",
               "views/redes/tecnico/material/faceplate_view.xml",
               "views/redes/tecnico/material/caja_view.xml",
               "views/redes/tecnico/material/otros_view.xml",
               "views/redes/tecnico/material/unidades_view.xml",
               
               "views/redes/tecnico/pedido/pedido_view.xml",
               "views/redes/tecnico/pedido/pedidopor_view.xml",
               "views/redes/tecnico/gestionequipos/gestionequipo_view.xml",
               "views/redes/tecnico/gestionequipos/area_view.xml",
               "views/redes/tecnico/archivo_view.xml",
               "views/redes/tecnico/trabajos_mandatorios_view.xml",
               "views/redes/tecnico/tareasolicitadaporr_view.xml",
               "views/redes/admin/pasantes_view.xml",
               "views/redes/admin/tecnicos_view.xml",
               
               
               "views/software/servidor_view.xml",
               "views/software/servidor_virtual_view.xml",
               "views/software/aplicacion_view.xml",
               "views/software/cuenta_view.xml",
               "views/software/cuentaBD_view.xml",
               "views/software/basedatos_view.xml",
               "views/software/cuenta_servidor_view.xml",
               "views/software/aplicacion_externa_view.xml",
               "views/software/cuenta_externa_view.xml",
               "views/software/aplicacion_escritorio_view.xml",
               "views/software/aplicacion_nube_view.xml",
               "views/software/autores_aplicacion_view.xml",
               "views/software/encargado1_view.xml",
               "views/software/rol_view.xml",
               
               "views/mantenimiento/dependencia_view.xml",
               "views/mantenimiento/subdependencia_view.xml",
               "views/mantenimiento/custodio_view.xml",
               "views/mantenimiento/equipomant_view.xml",
               "views/mantenimiento/mantenimiento_view.xml",
               "views/mantenimiento/responsable_mant_view.xml",
               "views/mantenimiento/propietarioequipo_view.xml",
               
               
               "views/telecom/equipos/router_view.xml",
               "views/telecom/equipos/ap_view.xml",
               "views/telecom/equipos/antena_view.xml",
               "views/telecom/equipos/equipoenlace_view.xml",
               "views/telecom/equipos/cableblindado_view.xml",
               "views/telecom/equipos/conectorblindado_view.xml",
               "views/telecom/equipos/base_view.xml",
               "views/telecom/equipos/telefonia_view.xml",
               "views/telecom/equipos/videoconferencia_view.xml",
               
               "views/telecom/actividades/actividad_view.xml",
               "views/telecom/pasantest_view.xml",
               "views/telecom/responsabletel_view.xml",
               "views/telecom/ubicaciontel_view.xml",

               "views/secretaria/seccion_view.xml",
               "views/secretaria/tareasecretaria_view.xml",
               "views/secretaria/tareasolicitadapors_view.xml",

               
               
               
               
    ],
    'installable': True,
    'active': False,
}



