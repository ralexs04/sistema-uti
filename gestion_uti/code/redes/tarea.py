from osv import osv, fields
import time
class tarea(osv.osv):
    _name = 'uti.tarea'
    _columns = {
          'name':fields.char('Tarea', size=200),
          'lugar':fields.char('Lugar',  size=100),
          'fechaP':fields.char('Fecha Publicacion'),
          'fechaI':fields.datetime('Fecha Inicio'),
          'fechaE':fields.datetime('Fecha Entrega'),
          'solucionado':fields.boolean('Solucionado'),
          'descripcion':fields.text('Descripcion'),
          'id_pasantes':fields.many2one('uti.pasantes', 'Pasante'),
          'id_tecnicos':fields.many2one('uti.tecnicos', 'Tecnico'),
          'solicitado_id':fields.many2one('uti.tareaporsecrr', 'Tarea Solicitada Por'),
        }

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
            
    _defaults = {
        'fechaP': time.strftime("%d/%m/%Y")+" "+time.strftime("%H:%M:%S"), 
}
        



    
tarea()

class trabajosMandatorios(osv.osv):
    _name = 'uti.tm'
    
    _columns = {
          'area':fields.many2one('uti.area', 'Area'),
          'name':fields.many2one('uti.ubicacion', 'Ubicacion'),
          'descripcion':fields.text('Descripcion'),
          'fechaP':fields.char('Fecha Publicacion'),
          'id_tecnicos':fields.many2one('uti.tecnicos', 'Tecnico Responsable'),
          'tipe_selection' : fields.selection([ ('1','ALTA'), ('2','MEDIA'), ('3','BAJA'), ],'Prioridad', required = True),
         
        }
        
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""      
    _defaults = {
      'fechaP': time.strftime("%d/%m/%Y"), 
}  
trabajosMandatorios()


