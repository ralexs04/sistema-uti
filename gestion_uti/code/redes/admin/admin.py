from osv import osv, fields

class admin(osv.osv):
    _name = 'uti.admin'
    _columns = {
          'name':fields.many2one('uti.tarea', 'Tarea'),
          'ubicacion_id':fields.many2one('uti.ubicacion', 'Ubicacion'),
          'equipo_id':fields.many2one('uti.equipo', 'Equipo'),
          'inventario_id':fields.many2one('curso.inventario', 'Inventario'),
          
        }

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""      
    
admin()

