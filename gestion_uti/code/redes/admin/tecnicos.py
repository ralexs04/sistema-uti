from osv import osv, fields
import time
class tecnicos(osv.osv):
    _name = 'uti.tecnicos'
    _inherit = 'uti.persona'
    _columns = {
         'persona_id':fields.many2one('uti.persona', 'Persona'),
         'fecha':fields.date('Fecha de Ingreso'),
         'tarea_id':fields.one2many('uti.tarea', 'id_tecnicos', 'Tareas Asignadas '),
         'trabajo_id':fields.one2many('uti.tm', 'id_tecnicos', 'Trabajos Mandatorios'),
        }

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
             
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}     
    
tecnicos()

