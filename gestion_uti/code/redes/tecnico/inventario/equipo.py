from osv import osv, fields
import re
class equipo(osv.osv):
    _name = 'uti.equipo'
    _columns = {
          'name':fields.char('Nombre', size=64),
          'marca':fields.char('Marca',  size=64 ,required=True),
          'modelo':fields.char('Modelo',  size=64 ),
          'mac':fields.char('Mac',  size=15 ),
          'serie':fields.char('Serie',  size=64),
          'ip':fields.char('Ip',  size=12),
          'rackeable':fields.boolean('Equipo Rackeable'),
          'codigo':fields.char('Codigo de Bodega', size=64),
        }  
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""

    def onchange_noletter(self, cr, uid, ids, valor,name,context=None):
      res = {'value':{}}
      if valor:
        cadena = valor
        bandera=False
        for i in cadena:
          if i.isalpha():
            bandera=True
            break
        if bandera:
          error_msg = 'Ingrese solo Numeros'
          raise osv.except_osv('Error en el campo '+name, error_msg )
      return res    

    def onchangue_validaip(self, cr, uid, ids, valor, name, context=None):
      if valor:
        cadena=valor

        bandera=False
        for i in cadena:
          if i.isalpha():
            bandera=True
            break
        if bandera:
          error_msg = 'Ingrese solo Numeros'
          raise osv.except_osv('Error en el campo : '+name, error_msg )
        else:
          if re.match('[(0-9)]+\.[(0-9)]+\.[(0-9)]+\.[(0-9)]',cadena.lower()):
            print "ip correcta"
          else:
            raise osv.except_osv(('Invalid'),('La Ip ingresada es Incorrecta')) 
      return ""  

    _defaults = {
        'ip': '0.0.0.0',
}  
equipo()

