from osv import osv, fields
import time
class inventario(osv.osv):
    _name = 'curso.inventario'
    _columns = {
          
          'name':fields.char('Fecha Ingreso'),
          'reference':fields.char('Referencia', size=100),
          'equipo_id':fields.many2one('uti.equipo', 'Equipo'),
          'ubicacion_id':fields.many2one('uti.ubicacion', 'Ubicacion'),
          'rack':fields.boolean('Se encuentra en Rack'),
          'reubicacion':fields.date('Fecha de Reubicacion'),
          'descripcion':fields.text(' '),
          #'cedula':fields.char('Cedula', size=10),
          #'edad':fields.integer('Edad'),
          #'nivel':fields.char('NivelFormacion', size=10),
          #'carrera_id':fields.many2one('curso.carrera', 'Carrera'),
          #'genero':fields.char('Genero', size=20),
          #'nacionalidad':fields.char('Nacionalidad', size=20),
#          'fecha_inicio': fields.date('Fecha Inicio'),
 #         'fecha_finalizacion': fields.date('Fecha Finalizacion'),
  #        'solicitud_id':fields.many2one('curso.solicitud','solicitud'),
        }

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""    
          
    _defaults = {
        'name': time.strftime("%d/%m/%Y"), 
}
    
inventario()

