from osv import osv, fields

class ubicacion(osv.osv):
    _name = 'uti.ubicacion'
    _columns = {
          'name':fields.char('Bloque',  size=64, required=True),
          'departamento':fields.char('Departamento',  size=64),
          'descripcion':fields.char('Descripcion', size=300),
         
        }  
    
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
        
ubicacion()
