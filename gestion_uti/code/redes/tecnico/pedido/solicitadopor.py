from osv import osv, fields
import time
class solicitadopor(osv.osv):
    _name = 'uti.solicitadopor'
    _inherit = 'uti.persona'
    _columns = {
         'fecha':fields.date('Fecha de Ingreso'),
         
        }
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""    
	          
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}
solicitadopor()
