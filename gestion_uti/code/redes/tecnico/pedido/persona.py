from osv import osv, fields
import re
class persona(osv.osv):
    _name = 'uti.persona'
    _columns = {
          'name':fields.char('Nombres y Apellidos', size=64,required=True),
          'cedula':fields.char('Cedula', size=10,required=True),
          'telefono':fields.integer('Telefono', size=64,required=True),
          'correo':fields.char('Correo', size=50,required=True),
          'descripcion':fields.text('Descripcion'),
          
        }

    def onchange_cedula(self, cr, uid, ids, valor,name,context=None):
      res = {'value':{}}
      if valor:

        cadena = valor
        bandera=False
        for i in cadena:
          if i.isalpha():
            bandera=True
            break
        if bandera:
          error_msg = 'Ingrese solo Numeros'
          raise osv.except_osv('Error en el campo '+name, error_msg )
        else:
          if((len(valor))==10):
              numeros = [int(s) for s in valor]
                  #Sin tomar en cuenta ultimo digito
              coefs = [2,1] * 4 + [2]
              mult = [ numeros[i]*c for i,c in enumerate(coefs) ]
              #Cifras mayores a 10
              mult = [ m if m < 10 else (m - 9) for m in mult ]
              #suma los elementos de una lista        
              residuo = sum(mult) % 10
              verificador = numeros[-1]
              if (10 - residuo) == verificador:
                  print'cedula correcta'
              else:
                  raise osv.except_osv(('invalid'),('La cedula es incorrecta'))
          else:
            raise osv.except_osv(('invalid'),('Debe ingresar los 10 digitos de la cedula'))  
      return res

    
   

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""

    def onchangue_validaCorreo(self, cr, uid, ids, valor, context=None):
      if valor:
        correo=valor
        if re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,4}$',correo.lower()):
          print "Correo correcto"
        else:
          raise osv.except_osv(('Invalid'),('El Correo es incorrecto')) 
      return ""



    
       
        
persona()

