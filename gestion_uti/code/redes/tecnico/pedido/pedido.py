from osv import osv, fields

class pedido(osv.osv):
    _name = 'uti.pedido'
    _columns = {
        
          'name':fields.char('Detalles Intalacion',  size=200),
          'fechaP':fields.date('Fecha Pedido'),
          'responsable_id':fields.many2one('uti.tecnicos', 'Responsable'),
          'pedidopor_id':fields.many2one('uti.solicitadopor', 'Solicitado Por'),
          'descripcion':fields.text('Descripcion', size=100),
          'area_id':fields.many2one('uti.area', 'Area'),
          'ubicacion_id':fields.many2one('uti.ubicacion', 'Ubicacion'),
          'fechaE':fields.datetime('Fecha Entrega'),
        } 

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""     
    
pedido()
