from osv import osv, fields

class area(osv.osv):
    _name = 'uti.area'
    
    _columns = {
         'name':fields.char('Area', size=200, required=True),
        }

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
                  
area()
