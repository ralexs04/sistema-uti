from osv import osv, fields

class gestionequipo(osv.osv):
    _name = 'uti.gestionequipo'
    _inherit = 'curso.inventario'
    
    _columns = {
         'inventario_id':fields.many2one('curso.inventario', 'Inventario'),
         'area_id':fields.many2one('uti.area', 'Area'),
        }

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
                  
gestionequipo()
