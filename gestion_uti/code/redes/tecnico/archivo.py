from osv import osv, fields
import time
class archivo(osv.osv):
    _name = 'uti.archivo'
    _columns = {
          'name':fields.char('Nombre', size=100),
          'adjuntar':fields.binary('Archivo'),
          'descripcion':fields.text('Descripcion'),
          'fechap':fields.char('Fecha Publicacion'),
        }  
        
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""

    _defaults = {
        'fechap': time.strftime("%d/%m/%Y"), 
}
archivo()
