from osv import osv, fields
import time
class conector(osv.osv):
    _name = 'uti.conector'
    _columns = {
          
          'marca':fields.char('Marca',  size=64, required=True),
          'cantidad':fields.char('Cantidad',  size=64, required=True),
          'unidades_id':fields.many2one('uti.unidades', 'Unidad'),
          'fecha':fields.char('Fecha Ingreso'),
          'tipoconector' : fields.selection([ ('1','PLASTICO'), ('2','BLINDADO') ],' Tipo'),
          'categoria_selection' : fields.selection([ ('1','5E'), ('2','6A'), ('3','6B'), ('4','7A') ],' Categoria'),
        }  

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
            
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}   
conector()

