from osv import osv, fields
import time
class patchpanel(osv.osv):
    _name = 'uti.patchpanel'
    _columns = {
          'modulos':fields.char('Modulos',  size=100, required=True),
          'solido':fields.boolean('Solido'),
          'modular':fields.boolean('Modular'),
          'puerto':fields.integer('Nro. Puertos',  size=25),
          'fecha':fields.char('Fecha Ingreso'),
          'categoria_selection' : fields.selection([ ('1','5E'), ('2','6A'), ('3','6B'), ('4','7A') ],' Categoria'),

        } 


    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
             
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}
patchpanel()

