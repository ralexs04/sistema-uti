from osv import osv, fields
import time
class rak(osv.osv):
    _name = 'uti.rack'
    _columns = {
          'cantidad':fields.char('Cantidad', required=True),
          'unidades':fields.many2one('uti.unidades', 'Unidad'),
          'abatible':fields.boolean('Abatible'),
          'fecha':fields.char('Fecha Ingreso'),
        } 

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
             
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}
rak()

