from osv import osv, fields
import time
class otros(osv.osv):
    _name = 'uti.otros'
    _columns = {
          'name':fields.char('Nombre Equipo',  size=64, required=True),
          'categoria':fields.char('Categoria',  size=64),
          'cantidad':fields.char('Cantidad',  size=64, required=True),
          'unidades_id':fields.many2one('uti.unidades', 'Unidad'),
          'marca':fields.char('Marca',  size=64, required=True),
          'descripcion':fields.text('Descripcion'),
          'fecha':fields.char('Fecha Ingreso'),
          
        }  


    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
        
            
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}
otros()

