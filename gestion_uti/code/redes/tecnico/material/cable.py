
from osv import osv, fields
import time
class cable(osv.osv):
    _name = 'uti.cable'
    _columns = {
          'name':fields.selection([ ('1','5E'), ('2','6A'), ('3','6B'), ('4','7A') ],' Categoria'),
          'cantidad':fields.char('Cantidad',  size=64, required=True),
          'unidades_id':fields.many2one('uti.unidades', 'Unidad'),
          'fecha':fields.char('Fecha Ingreso'),

        } 

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
        
            
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}
cable()
