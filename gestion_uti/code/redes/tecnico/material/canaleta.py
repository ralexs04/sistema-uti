
from osv import osv, fields
import time
class canaleta(osv.osv):
    _name = 'uti.canaleta'
    _columns = {
          'name':fields.char('Ancho',  size=64, required=True),
          'alto':fields.char('Alto',  size=64),
          'marca':fields.char('Marca',  size=64, required=True),
          'cantidad':fields.char('Cantidad',  size=64, required=True),
          'unidades_id':fields.many2one('uti.unidades', 'Unidad'),
          'fecha':fields.char('Fecha Ingreso'),
          'tipe_selection' : fields.selection([ ('1','ADHESIVO'), ('2','TORNILLO') ],' Tipo'),

        }  

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
            
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}
canaleta()
