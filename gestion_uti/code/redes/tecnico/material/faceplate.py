
from osv import osv, fields
import time
class faceplate(osv.osv):
    _name = 'uti.faceplate'
    _columns = {
          'cantidad':fields.char('Cantidad',  size=64, required=True),
          'unidades_id':fields.many2one('uti.unidades', 'Unidad'),
          'marca':fields.char('Marca',  size=64, required=True),
          'tipe_selection' : fields.selection([ ('1','DOBLES'), ('2','SIMPLES') , ('2','TRIPLES') ],'Tipo', required = True),
          'fecha':fields.char('Fecha Ingreso'),

        }  

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
            
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}
faceplate()
