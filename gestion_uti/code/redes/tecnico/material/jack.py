from osv import osv, fields
import time
class jack(osv.osv):
    _name = 'uti.jack'
    _columns = {
          'cantidad':fields.char('Cantidad',  size=64, required=True),
          'unidades_id':fields.many2one('uti.unidades', 'Unidad'),
          'marca':fields.char('Marca',  size=64, required=True),
          'categoria_selection' : fields.selection([ ('1','5E'), ('2','6A'), ('3','6B'), ('4','7A') ],' Categoria'),
          'fecha':fields.char('Fecha Ingreso'),

        }  

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
            
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}
jack()