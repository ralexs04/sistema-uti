
from osv import osv, fields

class unidades(osv.osv):
    _name = 'uti.unidades'
    _columns = {
          'name':fields.char('tipo',  size=64, required=True),
        } 

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
             
unidades()
