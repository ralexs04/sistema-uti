from osv import osv, fields
import smtplib , re
class tareasecretaria(osv.osv):
    _name = 'uti.tareasecretaria'
    _inherit = 'uti.tarea'
    
    _columns = {
         'solicitado_id':fields.many2one('uti.tareaporsecre', 'Tarea Solicitada Por'),
         'seccion_id':fields.many2one('uti.seccion1', 'Seccion'),
         'enviarCorreo':fields.boolean('Enviar Correo' , required=True),
         'mail':fields.char('Correo del Responsable de la Tarea' , required=True)
        }
    

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}


    def onchangue_validaCorreo(self, cr, uid, ids, valor, context=None):
      if valor:
        correo=valor
        if re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,4}$',correo.lower()):
          print "Correo correcto"
        else:
          raise osv.except_osv(('Invalid'),('El Correo es incorrecto')) 
      return ""
    

    def onchange_sendemail(self, cr, uid, ids, name, descripcion, correo, context=None):
        
        if correo:
        
            nombreTarea=str(name)
            descripcionTarea=str(descripcion)
            to = correo #a quien se le va a enviar el correo
            gmail_user = 'dominio@gmail.com' # quien envia el correo
            gmail_pwd = 'password' #pass
            smtpserver = smtplib.SMTP("smtp.gmail.com",587)
            smtpserver.ehlo()
            smtpserver.starttls()
            smtpserver.ehlo
            smtpserver.login(gmail_user, gmail_pwd)
            header = 'To:' + to + '\n' + 'From: ' + gmail_user + '\n' + 'Subject:Tarea Asignada Sistema UTI \n'
            print header
            #nombre = self.pool.get('uti.tareasecretaria').browse(cr, uid, uid).name]_id.id,
            msg = header + '\n\n Estimado (a) Ing, El sistema UTI le informa que tiene la siguiente Tarea Pendiente: \n\n NOMBRE TAREA ASIGNADA:\n\t '+ nombreTarea + '\n\n DESCRIPCION:\n\t' +descripcionTarea

            smtpserver.sendmail(gmail_user, to, msg)
            smtpserver.close()
        return ""


tareasecretaria()



