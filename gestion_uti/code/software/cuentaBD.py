from osv import osv,fields
import time
class cuentaBD(osv.osv):  
    _name = 'uti.cuentabd'
    _columns = {
            #'name':fields.char('Rol', size = 100),
            #'usuarios_id':fields.one2many('uti.usuario', 'aplicacion_id', 'Cuentas'),
            'username':fields.char('Nombre Usuario', size=50, required=True),
            'fecha':fields.char('Fecha Creacion '),
            #'clave':fields.char('Clave', size = 50),
            #'usuarios':fields.many2one('uti.servidor' , 'Servidor'),
            'id_rol':fields.many2one('uti.rol' , 'Rol',required=True),
            'id_basedatos':fields.many2one('uti.basedatos' , 'Base de Datos', required=True),
                }

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""            
                
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}
cuentaBD()


