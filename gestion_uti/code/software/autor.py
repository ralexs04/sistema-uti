from osv import osv, fields
import time
class autor(osv.osv):
    _name = 'uti.autor'
    _inherit = 'uti.persona'
    _columns = {
          'correo':fields.char('Correo', size=50 ,required=True),
          'id_aplicacion':fields.many2one('uti.aplicacion' , 'Aplicacion' ,required=True),
          'fecha':fields.char('Fecha Creacion '),
         # 'aplicacion_id':fields.many2one('uti.aplicacion' , 'Detalles'),
        } 

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
             
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}
autor()
