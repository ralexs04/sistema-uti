from osv import osv,fields
import time
class aplicacionNube(osv.osv):  
    _name = 'uti.aplicacionube'
    _columns = {
            'name':fields.char('Nombre Aplicacion', size = 100),
            'link':fields.char('Link Aplicacion', size = 200, required=True),
            'cuenta':fields.char('Cuentas Acesso', size = 300),
            'fecha':fields.char('Fecha Creacion '),
            'id_encargado':fields.many2one('uti.encargado' , 'Encargado' ,required=True),
            'cuentas_id':fields.one2many('uti.cuentaexterna', 'id_aplicacionexterna', 'Cuenta'),
            'nota':fields.text('Nota(s) '),
                }
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
                    
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}
aplicacionNube()




