from osv import osv,fields
import time
class basedatos(osv.osv):  
    _name = 'uti.basedatos'
    _columns = {
            'name':fields.char('Nombre Base de datos', size = 50 ,required=True),
            'tipo':fields.char('Tipo Base de datos', size = 50 ,required=True),
            'version':fields.char('Version', size = 50 ,required=True),
            'cuenta_id':fields.one2many('uti.cuentabd', 'id_basedatos', 'Cuenta' ,required=True),
            'fecha':fields.char('Fecha Creacion '),
            'nota':fields.text('Nota '),
            
                }

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
                    
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}
basedatos()

