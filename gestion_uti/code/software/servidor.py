from osv import osv, fields
import logging
import time,re
_logger = logging.getLogger(__name__)
class servidor(osv.osv):
    
    _name = 'uti.servidor'
    _columns = {
        
          'name':fields.char('Nombre', size=100  ),
          'ip_publica':fields.char('IP Publica', size=15, required=True,),
          'ip_privada':fields.char('IP Privada', size=15, required = True),
          'puerto':fields.char('Puerto ssh', size=25, required = True),
          
          'fecha':fields.char('Fecha Creacion '),
          'aplicaciones_id':fields.one2many('uti.aplicacion', 'id_servidor', 'Aplicacion'),
          'cuentas_id':fields.one2many('uti.cuentaservidor', 'id_servidor', 'Cuenta'),
          'nota':fields.text('Nota(s) '),
          
        }

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""     


    def onchange_noletter(self, cr, uid, ids, valor,name,context=None):
      res = {'value':{}}
      if valor:

        cadena = valor
        bandera=False
        for i in cadena:
          if i.isalpha():
            bandera=True
            break
        if bandera:
          error_msg = 'Ingrese solo Numeros'
          raise osv.except_osv('Error en el campo '+name, error_msg )
      return res

    def onchangue_validaip(self, cr, uid, ids, valor, name, context=None):
      if valor:
        cadena=valor

        bandera=False
        for i in cadena:
          if i.isalpha():
            bandera=True
            break
        if bandera:
          error_msg = 'Ingrese solo Numeros'
          raise osv.except_osv('Error en el campo : '+name, error_msg )
        else:
          if re.match('[(0-9)]+\.[(0-9)]+\.[(0-9)]+\.[(0-9)]',cadena.lower()):
            print "ip correcta"
          else:
            raise osv.except_osv(('Invalid'),('La Ip ingresada es Incorrecta')) 
      return ""



    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
        'ip_publica':'0.0.0.0',
        'ip_privada':'0.0.0.0'
}



servidor()



  
  


