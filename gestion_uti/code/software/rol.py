from osv import osv, fields
import time
class rol(osv.osv):
    _name = 'uti.rol'
    _columns = {
          'name':fields.char('Nombre Rol', size=50 ,required=True),
          'fecha':fields.char('Fecha Creacion '),
         # 'aplicacion_id':fields.many2one('uti.aplicacion' , 'Detalles'),
        } 

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
             
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}
rol()