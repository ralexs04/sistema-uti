from osv import osv,fields
import time

class aplicacionescritorio(osv.osv):  
    _name = 'uti.aplicacionescritorio'
    _columns = {
            'name':fields.char('Nombre Aplicacion', size = 100, required=True),
            'plataforma':fields.char('Plataforma', size = 200,required=True),
            'lenguaje':fields.char('Lenguaje Programacion', size = 300,required=True),
            'version':fields.char('Version', size = 300,required=True),
            'licencia':fields.char('Licencia', size = 300,required=True),
            'fecha':fields.char('Fecha Creacion'),
            #'usuarios_id':fields.one2many('uti.usuario', 'aplicacion_id', 'Cuentas'),
            
            'bd_id':fields.many2one('uti.basedatos', 'Base Datos'  ,required=True),
            'id_servidor':fields.many2one('uti.servidor' , 'Servidor Fisico' ),
            'id_servidorvirtual':fields.many2one('uti.servidorvirtual' , 'Servidor Virtual' ),
            'id_encargado':fields.many2one('uti.encargado', 'Encargado'  ,required=True),
            'cuenta_id':fields.one2many('uti.cuenta', 'id_aplicacion', 'Cuentas Acceso '),
            'autor_id':fields.one2many('uti.autor', 'id_aplicacion', 'Autor'),
            'descripcion':fields.text('Descripcion', size = 200),
                }
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
    }           
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""

    

aplicacionescritorio()




