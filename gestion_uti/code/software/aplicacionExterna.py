from osv import osv,fields
import time
class aplicacionexterna(osv.osv):  
    _name = 'uti.aplicacionexterna'
    _columns = {
            'name':fields.char('Nombre Aplicacion', size = 100, required=True),
            'link':fields.char('Link Aplicacion', size = 200 ,required=True),
            'entidad':fields.char('Entidad', size = 200 ,required=True),
            'cuenta':fields.char('Cuentas Acesso', size = 300 ),
            'fecha':fields.char('Fecha Creacion '),
            'id_encargado':fields.many2one('uti.encargado' , 'Encargado' ,required=True),
            'cuentas_id':fields.one2many('uti.cuentaexterna', 'id_aplicacionexterna', 'Cuenta'),
                }
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
                    
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
}

aplicacionexterna()




