from osv import osv,fields

class subdependencia(osv.osv):  
    _name = 'uti.subdependencia'
    _columns = {
            'name':fields.char('Nombre', size = 100),
            'codigo':fields.char('Codigo', size = 100),
            'id_dependencia':fields.many2one('uti.dependencia', 'Dependencia'),
            'custodio_id':fields.one2many('uti.custodio', 'id_subdependencia', 'Custodio'),
            
                }
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
        
                   
subdependencia()