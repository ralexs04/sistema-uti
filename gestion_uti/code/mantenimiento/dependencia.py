from osv import osv,fields

class dependencia(osv.osv):  
    _name = 'uti.dependencia'
    _columns = {
            'name':fields.char('Nombre', size = 100),
            'codigo':fields.char('Codigo', size = 100),
            'subdependencia_id':fields.one2many('uti.subdependencia', 'id_dependencia', 'Subdependencia'),
                }

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""            

dependencia()