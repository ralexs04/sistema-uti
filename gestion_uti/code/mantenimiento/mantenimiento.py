from osv import osv,fields

class mantenimiento(osv.osv):  
    _name = 'uti.mantenimiento'
    _inherit = 'uti.persona'
    _inherit = 'uti.custodio'
    _columns = {
            'id_equipomant':fields.many2one('uti.equipomant', 'Equipo'),
            'custodio_id':fields.many2one('uti.custodio', 'Custodio'),
            'fechallegada':fields.datetime('Fecha y Hora de Llegada'),
            'fechasalida':fields.datetime('Fecha y Hora de Salida'),
            'problema':fields.text('Problema Presentado' ),
            'observacion':fields.text('Obervaciones' ),
            'accion':fields.text('Acciones Realizadas' ),
            'responsablemant_id':fields.many2one('uti.responsablemant', 'Responsable'),
            'tipoatencion' : fields.selection([ ('1','VISITA'), ('2','ATENCION EN RECEPCION'), ],'Tipo', required = True),
                }

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
                    
mantenimiento()