from osv import osv,fields

class equipomant(osv.osv):  
    _name = 'uti.equipomant'
    _columns = {
            'name':fields.char('Codigo Equipo', size = 100),
            'codebodega':fields.char('Codigo Bodega', size = 100, ),
            'marca':fields.char('Marca', size = 100, required=True),
            'modelo':fields.char('Modelo', size = 100),
            'numserie':fields.char('Numero Serie', size = 100),
            'procesador':fields.char('Procesador', size = 100),
            'memoria':fields.char('Memoria', size = 100),
            'disco':fields.char('Disco', size = 100),
            'mac':fields.char('Mac', size = 100),
            'ip':fields.char('ip', size = 15),

            'propietarioequipo_id':fields.many2one('uti.propietarioequipo', 'Propietario'),
            'descripcion':fields.text('Descripcion'),
            'id_custodio':fields.many2one('uti.custodio', 'Custodio'),
                }
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""            

equipomant()
