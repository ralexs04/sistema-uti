from osv import osv,fields

class custodio(osv.osv):  
    _name = 'uti.custodio'
    _columns = {
            'name':fields.char('Nombre', size = 100),
            'cedula':fields.char('Nro. CI', size = 10, ),
            'tipo':fields.char('Tipo', size = 100),
            'cargo':fields.char('Cargo', size = 100),
            'id_subdependencia':fields.many2one('uti.subdependencia', 'Subdependencia'),
            'equipo_id':fields.one2many('uti.equipomant', 'id_custodio', 'Equipo'),
                }

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
	

	
custodio()