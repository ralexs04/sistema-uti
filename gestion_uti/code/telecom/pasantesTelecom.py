from osv import osv, fields
class pasantesTelecom(osv.osv):
    _name = 'uti.pasantest'
    _inherit = 'uti.persona'
    _columns = {
         'persona_id':fields.many2one('uti.persona', 'Persona'),
         'correo':fields.char('Correo', size=75),
         'fecha':fields.date('Fecha Ingreso'),
         'actividad_id':fields.one2many('uti.actividadt', 'id_pasantes', 'Actividades Asignadas '),
        }  
        
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return "" 
pasantesTelecom()