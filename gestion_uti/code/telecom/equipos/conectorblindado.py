
from osv import osv, fields

class conectorblindado(osv.osv):
    _name = 'uti.conectorblindado'
    _columns = {
           'categoria':fields.char('Categoria', size=64),
           'cantidad':fields.char('Cantidad', required=True),
           'unidades':fields.many2one('uti.unidades', 'Unidad', required=True),
        }  

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return "" 
        
conectorblindado()
