from osv import osv, fields
import time,re
class telefonia(osv.osv):
    _name = 'uti.telefonia'
    _columns = {
          'cantidad':fields.char('Cantidad', required=True),
          'unidades':fields.many2one('uti.unidades', 'Unidad', required=True),
          'marca':fields.char('Marca', required=True),
          'ip':fields.char('Ip Administrativa' , required=True),
          'extencionesip':fields.char('Extenciones IP'),
          'analogicas':fields.char('Analogicas'),
          'troncales':fields.char('Troncales'),
          'fecha':fields.char('Fecha Ingreso'),
        } 

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
    def onchangue_validaip(self, cr, uid, ids, valor, name, context=None):
      if valor:
        cadena=valor

        bandera=False
        for i in cadena:
          if i.isalpha():
            bandera=True
            break
        if bandera:
          error_msg = 'Ingrese solo Numeros'
          raise osv.except_osv('Error en el campo : '+name, error_msg )
        else:
          if re.match('[(0-9)]+\.[(0-9)]+\.[(0-9)]+\.[(0-9)]',cadena.lower()):
            print "ip correcta"
          else:
            raise osv.except_osv(('Invalid'),('La Ip ingresada es Incorrecta')) 
      return ""         
    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
        'ip':'0.0.0.0'
}
telefonia()

