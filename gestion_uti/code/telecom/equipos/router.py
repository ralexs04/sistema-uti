
from osv import osv, fields

class router(osv.osv):
    _name = 'uti.router'
    _inherit = 'uti.equipo'
    _columns = {
    	'frecuencia':fields.char('Frecuencia', required=True),
                

        }
        
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""       
    
router()        
