from osv import osv, fields
import time,re
class videoconferencia(osv.osv):
    _name = 'uti.videoconferencia'
    _columns = {
          'name':fields.char('Marca', required=True),
          'ip':fields.char('Ip Administrativa' , required=True),
          'tipo' : fields.selection([ ('1','PUNTO A PUNTO'), ('2','MULTIPUNTO') ],' Tipo'),
          'descripcion':fields.text('Descripcion'),
          'fecha':fields.char('Fecha Ingreso'),
        } 

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""
        
    def onchangue_validaip(self, cr, uid, ids, valor, name, context=None):
      if valor:
        cadena=valor

        bandera=False
        for i in cadena:
          if i.isalpha():
            bandera=True
            break
        if bandera:
          error_msg = 'Ingrese solo Numeros'
          raise osv.except_osv('Error en el campo : '+name, error_msg )
        else:
          if re.match('[(0-9)]+\.[(0-9)]+\.[(0-9)]+\.[(0-9)]',cadena.lower()):
            print "ip correcta"
          else:
            raise osv.except_osv(('Invalid'),('La Ip ingresada es Incorrecta')) 
      return "" 

    _defaults = {
        'fecha': time.strftime("%d/%m/%Y"), 
        'ip':'0.0.0.0'
}
videoconferencia()