
from osv import osv, fields

class antena(osv.osv):
    _name = 'uti.antena'
    _inherit = 'uti.equipo'
    _columns = {
                'name':fields.char('Frecuencia', size=64),
                 'potencia':fields.char('Potencia', size=64),

        }  
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""     
    
antena()
