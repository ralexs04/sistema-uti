
from osv import osv, fields

class ap(osv.osv):
    _name = 'uti.ap'
    _inherit = 'uti.equipo'
    _columns = {
                'name':fields.char('Codigo de Bodega', size=64),
                'bandas' : fields.selection([ ('1','DUAL BAND'), ('2','24'), ('3','58') ],' Banda'),

        }  

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""     
    
ap()
