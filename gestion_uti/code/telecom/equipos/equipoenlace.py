from osv import osv, fields

class equipoenlace(osv.osv):
    _name = 'uti.equipoenlace'
    _inherit = 'uti.equipo'
    _columns = {
            'id_area':fields.many2one('uti.area', 'Area'),
    	    'id_ubicaciont':fields.many2one('uti.ubicaciontel', 'Ubicacion'),
        }  
          
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return "" 
equipoenlace()
