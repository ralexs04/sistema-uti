
from osv import osv, fields

class base(osv.osv):
    _name = 'uti.base'
    _inherit = 'uti.ubicacion'
    _columns = {
                'infraestructura' : fields.selection([ ('1','BASE'), ('2','TORRE'), ('3','BRAZO'), ],'Infraestructura', required = True),
                'area' : fields.selection([ ('1','AGROPECUARIA'), ('2','EDUCATIVA'), ('3','ENERGIA'),('4','MED'),('4','MOTUPE'), ('5','SALUD'), ],'Area', required = True),

        }

    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""       
    
base()
