from osv import osv, fields

class actividadt(osv.osv):
    _name = 'uti.actividadt'
    _inherit = 'uti.tarea'
    _columns = {
          'name':fields.char('Nombre Actividad', size=200),
          'id_area':fields.many2one('uti.area' , 'Area'),
          'reubicado':fields.boolean('Reubicado'),
          'descripcion':fields.text('Descripcion de Reubicacion'),
          'observacion':fields.text('Observacion'),
          'id_responsable':fields.many2one('uti.tecnicos' , 'Tecnico'),
          'id_pasantes':fields.many2one('uti.pasantest' , 'Pasante'),
          'id_ubicaciontel':fields.many2one('uti.ubicaciontel' , 'Ubicacion'),

        } 
    def onchange_upper(self, cr, uid, ids, valor, name, context=None):
        if valor:
            vals = {name: valor.upper()}        
            return {'value': vals}
        return ""   
                    
      
actividadt()
